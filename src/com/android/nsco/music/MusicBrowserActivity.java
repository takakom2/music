/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.nsco.music;

import com.android.nsco.music.MusicUtils.ServiceToken;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MusicBrowserActivity extends Activity
implements MusicUtils.Defs, OnItemClickListener {
	static String[] menuItems = { "Item 1", "Item 2", "Item 3" };
	static ArrayAdapter<String> adapter;

	ListView drawerListView;
	ActionBarDrawerToggle drawerToggle;
	TextView textView1;

	DrawerLayout drawerLayout;

	private ServiceToken mToken;

	public MusicBrowserActivity() {
	}

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawer;
	private final String LOGTAG = "MusicApp";

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		//        int activeTab = MusicUtils.getIntPref(this, "activetab", R.id.artisttab);
		//        if (activeTab != R.id.artisttab
		//                && activeTab != R.id.albumtab
		//                && activeTab != R.id.songtab
		//                && activeTab != R.id.playlisttab) {
		//            activeTab = R.id.artisttab;
		//        }
		//        MusicUtils.activateTab(this, activeTab);
		//
		//        String shuf = getIntent().getStringExtra("autoshuffle");
		//        if ("true".equals(shuf)) {
		//            mToken = MusicUtils.bindToService(this, autoshuffle);
		//        }
		//    }
		setContentView(R.layout.top_layout);
		findViews();
		setListView();
		setDrawer();
	}

	@Override
	public void onDestroy() {
		if (mToken != null) {
			MusicUtils.unbindFromService(mToken);
		}
		super.onDestroy();
	}

	private final ServiceConnection autoshuffle = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName classname, IBinder obj) {
			// we need to be able to bind again, so unbind
			try {
				unbindService(this);
			} catch (final IllegalArgumentException e) {
			}
			final IMediaPlaybackService serv = IMediaPlaybackService.Stub.asInterface(obj);
			if (serv != null) {
				try {
					serv.setShuffleMode(MediaPlaybackService.SHUFFLE_AUTO);
				} catch (final RemoteException ex) {
				}
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName classname) {
		}
	};

	protected void findViews() {
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerListView = (ListView) findViewById(R.id.drawer_listview);
		textView1 = (TextView) findViewById(R.id.textView1);
	}

	protected void setListView(){
		// データアダプタ
		adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, menuItems); drawerListView.setAdapter(adapter);
		// アイテムの選択
		drawerListView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		textView1.setText((position + 1) + "");
		drawerLayout.closeDrawers();
	}

	protected void setDrawer() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_menu_shuffle, R.string.drawer_open,
				R.string.drawer_close);
		drawerLayout.setDrawerListener(drawerToggle);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}
}

